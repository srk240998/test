FROM python:3.10

# Installing required Python libraries
WORKDIR /app/test/
RUN pip install -r requirements.txt

RUN apt-get update && apt-get install -y stress

CMD [ "python", "Main.py"]
